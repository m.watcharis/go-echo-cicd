package repositories

import "context"

type Repositories interface {
	GetNumber(ctx context.Context) ([]int, error)
}

type repositories struct{}

func NewRepositories() Repositories {
	return &repositories{}
}

func (r *repositories) GetNumber(ctx context.Context) ([]int, error) {
	n := 10
	storeNumber := make([]int, 0)
	for i := 0; i < n; i++ {
		storeNumber = append(storeNumber, i)
	}
	return storeNumber, nil
}
