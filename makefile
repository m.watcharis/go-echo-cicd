mock:
	- mockgen -package=mocks -source=repositories/repositories.go -destination=repositories/mocks/repositories.go

test:
	- go test ./...

test-services:
	- go test ./services/*