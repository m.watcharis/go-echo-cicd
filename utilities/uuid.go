package utilities

import (
	"encoding/base64"
	"strconv"
	"strings"
	"time"
)

func RandomTraceID() string {
	now := time.Now().UnixNano()
	data := strconv.Itoa(int(now))
	result := strings.ToLower(base64.StdEncoding.EncodeToString([]byte(data)))
	return result[:len(result)-2]
}
