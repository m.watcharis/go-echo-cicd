FROM golang:1.19-alpine AS monlybuilder

WORKDIR /app

COPY go.mod .
COPY go.sum .

RUN go mod download

COPY ./ ./

# complier go to static application binary
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -a -o main main.go

FROM alpine:3.17.0

WORKDIR /app

# Copy build result from monlybuilder
COPY --from=monlybuilder /app/main .

EXPOSE 1234

ENTRYPOINT ["./main"]