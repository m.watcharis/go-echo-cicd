package router

import (
	"github.com/labstack/echo/v4"
	"gitlab.watcharis/go-echo-cicd/repositories"
	"gitlab.watcharis/go-echo-cicd/services"
)

func InitRouter(e *echo.Echo) *echo.Group {
	main := e.Group("/go-echo-cicd")
	api := main.Group("/api")
	g := api.Group("/v1")

	repo := repositories.NewRepositories()
	svc := services.NewServices(repo)
	handler := NewHandler(svc)

	g.GET("/get-number", handler.GetNumber)
	return g
}
