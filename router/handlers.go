package router

import (
	"context"
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.watcharis/go-echo-cicd/models"
	"gitlab.watcharis/go-echo-cicd/services"
	"gitlab.watcharis/go-echo-cicd/utilities"
)

type Handler interface {
	GetNumber(c echo.Context) error
}

type handler struct {
	svc services.Service
}

func NewHandler(svc services.Service) Handler {
	return &handler{
		svc: svc,
	}
}

func (h *handler) GetNumber(c echo.Context) error {
	ctx := context.WithValue(c.Request().Context(), "trace_id", utilities.RandomTraceID())
	result, err := h.svc.GetNumber(ctx)
	if err != nil {
		return c.JSON(http.StatusBadRequest, models.Response{
			Success:      false,
			ErrorMessage: err,
		})
	}
	return c.JSON(http.StatusOK, models.Response{
		Success: true,
		Data:    result,
	})
}
