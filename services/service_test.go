package services

import (
	"context"
	"errors"
	"fmt"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/suite"
	"gitlab.watcharis/go-echo-cicd/repositories/mocks"
)

type ServiceTestSuite struct {
	suite.Suite
	ctx              context.Context
	mockRepositories *mocks.MockRepositories
	undertest        Service
}

func (s *ServiceTestSuite) SetupTest() {
	ctrl := gomock.NewController(s.T())
	defer ctrl.Finish()
	s.ctx = context.Background()
	s.mockRepositories = mocks.NewMockRepositories(ctrl)
	s.undertest = NewServices(s.mockRepositories)
}

func TestServiceTestSuite(t *testing.T) {
	suite.Run(t, new(ServiceTestSuite))
}

func (s *ServiceTestSuite) TestGetNumberCaseSuccess() {
	mockResult := []int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
	s.mockRepositories.EXPECT().GetNumber(s.ctx).Return(mockResult, nil)
	result, err := s.undertest.GetNumber(s.ctx)
	fmt.Println("result :", result)
	s.Assert().Equal(nil, err)
	s.Assert().Equal(mockResult, result)
}

func (s *ServiceTestSuite) TestGetNumberCaseError() {
	mockResult := []int{}
	s.mockRepositories.EXPECT().GetNumber(s.ctx).Return(mockResult, errors.New("Not found data"))
	result, err := s.undertest.GetNumber(s.ctx)
	fmt.Println("result :", result)
	s.Assert().NotEqual(nil, err)
	s.Assert().NotEqual(mockResult, result)
}
