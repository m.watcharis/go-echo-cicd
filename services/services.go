package services

import (
	"context"

	"gitlab.watcharis/go-echo-cicd/repositories"
)

type Service interface {
	GetNumber(ctx context.Context) ([]int, error)
}

type service struct {
	repo repositories.Repositories
}

func NewServices(repo repositories.Repositories) Service {
	return &service{
		repo: repo,
	}
}

func (s *service) GetNumber(ctx context.Context) ([]int, error) {
	result, err := s.repo.GetNumber(ctx)
	if err != nil {
		return nil, err
	}
	return result, nil
}
